import { Component, OnInit, inject } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { RouterLink } from '@angular/router';
@Component({
  selector: 'app-list-books',
  standalone: true,
  imports: [CommonModule, HttpClientModule, RouterLink],
  templateUrl: './list-books.component.html',
  styleUrl: './list-books.component.css'
})
export class ListBooksComponent implements OnInit {
  httpClient =inject(HttpClient)
  books: any[] = []
  fetchBooks() {
    this.httpClient

    .get("https://openlibrary.org/authors/OL2162284A/works.json")
    .subscribe((data: any) => {
      this.books = data.entries
    })
  }

ngOnInit(): void {
  this.fetchBooks()
}

}
