import { Component, OnInit, inject } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail-book',
  standalone: true,
  imports: [CommonModule, HttpClientModule],
  templateUrl: './detail-book.component.html',
  styleUrl: './detail-book.component.css',
})
export class DetailBookComponent implements OnInit {
  httpClient = inject(HttpClient);
  book: any = {};
  key: string | null = "" 
  constructor(
    private route: ActivatedRoute
  ) {}

  fetchBook() {
    this.httpClient
      .get('https://openlibrary.org/works/OL8200891W' + '.json')
      .subscribe((data: any) => {
        this.book = data;
        typeof(this.book.description) == "string" ? this.book.description = data.description : this.book.description = data.description.value
      });
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      // c'est identifiant va me permettre de charger les détails du livre correspondant
      this.key = params.get('bookKey');
      console.log(this.key)
    });

    this.fetchBook();
  }
}
