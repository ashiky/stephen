import { Routes } from '@angular/router';
import { DetailBookComponent } from './detail-book/detail-book.component';
import { ListBooksComponent } from './list-books/list-books.component';
export const routes: Routes = [
    { path: '', component: ListBooksComponent },
    { path: 'details/:bookKey', component: DetailBookComponent },
];
